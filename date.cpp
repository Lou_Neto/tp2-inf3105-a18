/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP2                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp2/              *
 *  Complété par:                                        *
 *   1) Lou-Gomes Neto NETL14039105                      *
 *   2) Hugo Twigg-Coté TWIH25048700                     *
 *                                                       */
#include "date.h"
#include <cstdio>
#include <iomanip>
#include <assert.h>

Date::Date(const Date& autre) 
    : jours(autre.jours), heures(autre.heures), minutes(autre.minutes)
{
}

bool Date::operator <(const Date& d) const{
    if(jours == d.jours) {
        if(heures == d.heures) 
            return (minutes < d.minutes);
        else if(heures < d.heures) 
            return true;      
    }else if(jours < d.jours){
        return true;
    }
    return false;
}

bool Date::operator >(const Date& d) const{
    if(jours == d.jours){
        if(heures == d.heures)
            return(minutes > d.minutes);
        else if(heures > d.heures)
            return true;
    } else if (jours > d.jours){
        return true;
    }
    return false;
}

bool Date::operator <=(const Date& d) const{
    if(jours == d.jours) {
        if(heures == d.heures) 
            return (minutes <= d.minutes);
        else if(heures < d.heures) 
            return true;
    }else if(jours < d.jours){
        return true;
    }
    return false;
}

bool Date::operator ==(const Date& d) const{
    return (jours == d.jours && heures == d.heures && minutes == d.minutes);
}

std::ostream& operator << (std::ostream& os, const Date& d){
    int jours, heures, minutes;
    jours = d.jours;
    heures = d.heures;
    minutes = d.minutes;
    char chaine[40];
    sprintf(chaine, "%dj_%02dh%02dm", jours, heures, minutes);
    os << chaine;
    return os;
}

std::istream& operator >> (std::istream& is, Date& d){
    // char chaine[40];
    int jours, heures, minutes;
    char j, m, h, underscore;
    is >> jours >> j >> underscore >> heures >> h >> minutes >> m;
    assert(j=='j');
    assert(underscore=='_');
    assert(h=='h' && m=='m');

    d.jours = jours;
    d.heures = heures;
    d.minutes = minutes;

    return is;
}

