/* UQAM / Département d'informatique
   INF3105 - Structures de données et algorithmes
   Squelette pour classe générique ArbreMap<K,V> pour le Lab8 et le TP2.

   AUTEUR(S):
    1) Lou-Gomes Neto NETL14039105
    2) Hugo Twigg-Coté TWIH25048700
*/

#if !defined(__ARBREMAP_H__)
#define __ARBREMAP_H__

#include "arbreavl.h"

template <class K, class V>
class ArbreMap
{
    class Entree {
        public:
            Entree(const K& c):cle(c), valeur(){}
            K cle;
            V valeur;
            bool operator < (const Entree& e) const { return cle < e.cle; }
    };
    ArbreAVL<Entree> entrees;

  public:

    bool contient(const K&) const;
    void enlever(const K&);
    void vider();
    bool vide() const;

    const V& operator[] (const K&) const;
    V& operator[] (const K&);

    class Iterateur {
        public:
            Iterateur(const ArbreMap& a): iter(a.entrees.debut()) {}
            Iterateur(typename ArbreAVL<Entree>::Iterateur i) : iter(i) {}

            operator bool() const {
                return iter.operator bool();
            }
            Iterateur& operator++() {
                ++iter;
                return *this;
            }

            const K& cle() const {
                return (*iter).cle;
            }
            V& valeur() const {
                return (V&)(*iter).valeur;
            }

        private:
            typename ArbreAVL<Entree>::Iterateur iter;
    };

    Iterateur debut() { 
        return Iterateur(*this);
    }
    Iterateur fin() {
        return Iterateur(entrees.fin());
    }
    Iterateur rechercher(const K& cle) {
        return Iterateur(entrees.rechercher(cle));
    }
    Iterateur rechercherEgalOuSuivant(const K& cle) {
        return Iterateur(entrees.rechercherEgalOuSuivant(cle));
    }
    Iterateur rechercherEgalOuPrecedent(const K& cle) {
        return Iterateur(entrees.rechercherEgalOuPrecedent(cle));
    }

};

template <class K, class V>
void ArbreMap<K,V>::vider(){
    entrees.vider();
}

template <class K, class V>
bool ArbreMap<K,V>::vide() const{
    return entrees.vide();
}

template <class K, class V>
void ArbreMap<K,V>::enlever(const K& c)
{
    Entree entree(c);
    entrees.enlever(entree);
}

template <class K, class V>
bool ArbreMap<K,V>::contient(const K& c) const
{
    Entree entree(c);
    return entrees.contient(entree);
}

template <class K, class V>
const V& ArbreMap<K,V>::operator[] (const K& c)  const
{
    typename ArbreAVL<Entree>::Iterateur iter = entrees.rechercher(c);
    return entrees[iter].valeur;
}
 
template <class K, class V>
V& ArbreMap<K,V>::operator[] (const K& c) 
{
    typename ArbreAVL<Entree>::Iterateur iter = entrees.rechercher(Entree(c));
    if(!iter){
        entrees.inserer(Entree(c));
        iter = entrees.rechercher(c);
    }
    return entrees[iter].valeur;
}

#endif

