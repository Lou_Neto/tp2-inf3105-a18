/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP2                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp2/              *
 *  Complété par:                                        *
 *   1) Lou-Gomes Neto NETL14039105                      *
 *   2) Hugo Twigg-Coté TWIH25048700                     *
 *                                                       */
#if !defined(__DATE__H__)
#define __DATE__H__
#include <iostream>

// À compléter. Inspirez-vous de votre classe Heure du TP1.
class Date{
  public:
    Date() : jours(0), heures(0), minutes(0) {}
    Date(int jours, int heures, int minutes) 
      : jours(jours), heures(heures), minutes(minutes){}
    Date(const Date&);
    
    bool operator <(const Date& date) const;
    bool operator >(const Date& date) const;

    bool operator <=(const Date& date) const;
    bool operator ==(const Date& date) const;
  
    int jours, heures, minutes;

  private:
    
    
  friend std::ostream& operator << (std::ostream&, const Date& date);
  friend std::istream& operator >> (std::istream&, Date& date);
};

#endif

