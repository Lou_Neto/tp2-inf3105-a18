/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP2                                   *
 *  Auteur(s):                                           *
 *   1) Lou-Gomes Neto NETL14039105                      *
 *   2) Hugo Twigg-Coté TWIH25048700                     *
 *                                                       */
#include "succ.h"

bool Succursale::verifier(const Date &debut, const Date &fin)
{
    ArbreMap<Date, int>::Iterateur iter = reservations.rechercherEgalOuPrecedent(debut);
    if(!iter && nbVoitures == 0) return false;
    if(!iter) iter = reservations.debut();

    while (iter && iter.cle() <= fin)
    {
        if(iter.cle() < fin && iter.valeur() < 1) return false;
        if(iter.cle() == fin && iter.valeur() >= nbPlacesTotal) return false;
        ++iter;
    }
    return true;
}

bool Succursale::verifierLocation(const Date &debut)
{
    ArbreMap<Date, int>::Iterateur iter = reservations.rechercherEgalOuPrecedent(debut);
    if(!iter && nbVoitures == 0) return false;
    if(!iter) iter = reservations.debut();
    while (iter)
    {
        if (iter.valeur() < 1)
            return false;
        ++iter;
    }
    return true;
}

bool Succursale::verifierRetour(const Date &fin)
{
    ArbreMap<Date, int>::Iterateur iter = reservations.rechercherEgalOuPrecedent(fin);
    if(!iter && nbPlacesLibres == 0) return false;
    if(!iter) iter = reservations.debut();
    while(iter)
    {
        if (!(iter.valeur() < nbPlacesTotal)) 
            return false;
        ++iter;
    }
    return true;
}

void Succursale::enregistrer(const Date &debut, const Date &fin)
{
    ArbreMap<Date, int>::Iterateur iter = reservations.rechercherEgalOuPrecedent(debut);
    if(!iter){
        reservations[debut] = nbVoitures;
        iter = reservations.rechercher(debut);
    }else{
        reservations[debut] = iter.valeur();
        iter = reservations.rechercher(debut);
    }
    
    ArbreMap<Date, int>::Iterateur trouverFin = reservations.rechercherEgalOuPrecedent(fin);
    reservations[fin] = trouverFin.valeur();

    iter = reservations.rechercherEgalOuPrecedent(debut);
    while(iter && iter.cle() < fin)
    {
        iter.valeur()--;
        ++iter;
    }
}

void Succursale::enregistrerLocation(const Date &debut)
{
    ArbreMap<Date, int>::Iterateur iter = reservations.rechercherEgalOuPrecedent(debut);
    if(!iter){
        reservations[debut] = nbVoitures;
        iter = reservations.rechercher(debut);
    }else if(!(iter.cle() == debut)) {
        reservations[debut] = iter.valeur();
        iter = reservations.rechercher(debut);
    }

    while(iter)
    {
        iter.valeur()--;
        ++iter;
    }
}

void Succursale::enregistrerRetour(const Date &fin)
{
    ArbreMap<Date, int>::Iterateur iter = reservations.rechercherEgalOuPrecedent(fin);
    if(!iter){
        reservations[fin] = nbVoitures;
        iter = reservations.rechercher(fin);
    }else if(!(iter.cle() == fin)) {
        reservations[fin] = iter.valeur();
        iter = reservations.rechercher(fin);
    }
    
    while(iter)
    {
        iter.valeur()++;
        ++iter;
    }
}
