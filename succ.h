/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP2                                   *
 *  AUTEUR(S):                                           *
 *   1) Lou-Gomes Neto NETL14039105                      *
 *   2) Hugo Twigg-Coté TWIH25048700                     *
 *                                                       */
#if !defined(__SUCC_H__)
#define __SUCC_H__
#include <string>
#include "arbreavl.h"
#include "arbremap.h"
#include "date.h"
#include "pointst.h"

// using namespace std;

class Succursale{
  public:
    Succursale(PointST &p, int voitures, int places) 
      : emplacement(p), nbVoitures(voitures), 
        nbPlacesLibres(places), nbPlacesTotal(voitures+places) {}

    PointST emplacement;
    int nbVoitures, nbPlacesLibres, nbPlacesTotal;
    ArbreMap<Date, int> reservations;
 
    bool verifier(const Date&, const Date&);
    bool verifierLocation( const Date&);
    bool verifierRetour(const Date&);

    void enregistrer(const Date&, const Date&);
    void enregistrerLocation(const Date&);
    void enregistrerRetour(const Date&);

  private:
};

#endif

