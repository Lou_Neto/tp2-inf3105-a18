/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP2                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp2/              *
 *  Complété par:                                        *
 *   1) Lou-Gomes Neto NETL14039105                      *
 *   2) Hugo Twigg-Coté TWIH25048700                     *
 *                                                       */
#include <fstream>
#include <iostream>
#include <string>
#include "succ.h"
#include "pointst.h"
#include "arbremap.h"
// using namespace std;
#define INFINI 1.79769e+308

int tp2(std::istream& entree){
    
    ArbreMap<std::string, Succursale*> succursales;
    
    int id=1;
    while(entree)
    {
        std::string commande;
        entree >> commande >> std::ws;
        if(!entree) break;
        std::cout << id << " : ";
       
        if(commande=="creer")
        {
            std::string nom;
            PointST p;            
            int nbVoitures=0, nbPlacesLibres=0;
            entree >> nom >> p >> nbVoitures >> nbPlacesLibres;
            succursales[nom] = new Succursale(p, nbVoitures, nbPlacesLibres);
            std::cout << "Creee"  << std::endl;
        }
        else if(commande=="reserver")
        {
            std::string origine, destination;
            Date debut, fin;
            entree >> origine >> debut >> destination >> fin;
            bool ok = false;

            ArbreMap<std::string, Succursale*>::Iterateur succursaleOrigine = succursales.rechercher(origine);

            if(origine == destination) 
            {
                ok = succursaleOrigine.valeur()->verifier(debut,fin);
                if(ok) succursaleOrigine.valeur()->enregistrer(debut,fin);          
            }
            else
            {
                if (succursaleOrigine.valeur()->verifierLocation(debut))
                {
                    ArbreMap<std::string, Succursale*>::Iterateur succursaleDestination = succursales.rechercher(destination);
                    if(succursaleDestination.valeur()->verifierRetour(fin)) {
                        succursaleOrigine.valeur()->enregistrerLocation(debut);
                        succursaleDestination.valeur()->enregistrerRetour(fin);
                        ok = true;
                    }
                }
            }

            std::cout << (ok ? "Acceptee" : "NonDispo") << std::endl;    
        }
        else if(commande=="suggerer")
        {
            PointST origine, destination;
            Date debut, fin;
            entree >> origine >> debut >> destination >> fin;
            bool okLocation = false, okRetour = false;

            std::string meilleurSuccursaleAller, meilleurSuccursaleRetour;
            
            double meilleurAller = INFINI,
                meilleurRetour = INFINI,
                distanceAller, distanceRetour;
            
            ArbreMap<std::string, Succursale*>::Iterateur iter = succursales.debut();

            for(; iter; ++iter) { 
                if(iter.valeur()->verifierLocation(debut)) {
                    distanceAller = distance(origine, iter.valeur()->emplacement);
                    if(distanceAller < meilleurAller)
                    {
                        meilleurAller = distanceAller;
                        meilleurSuccursaleAller = iter.cle();
                        okLocation = true;
                    }
                }
                if(iter.valeur()->verifierRetour(fin)) {
                    distanceRetour = distance(iter.valeur()->emplacement, destination);
                    if(distanceRetour < meilleurRetour)
                    {
                        meilleurRetour = distanceRetour;
                        meilleurSuccursaleRetour = iter.cle();
                        okRetour = true;
                    }
                }
            }
            
            if(okLocation && okRetour) {
                std::cout << meilleurSuccursaleAller << " " << meilleurSuccursaleRetour << std::endl;
            }else{
                std::cout << "Impossible" << std::endl;
            }
        }
        else
        {
            std::cout << "Commande '" << commande << "' invalide!" << std::endl;
            return 2;
        }
        char pointvigule=0;
        entree >> pointvigule >> std::ws;
        if(pointvigule!=';'){
            std::cout << "Fichier d'entrée invalide!" << std::endl;
            return 3;
        }
        id++;
    }
    return 0;
}

// syntaxe d'appel : ./tp2 [nomfichier.txt]
//   ==> argc=2 argv[0]="./tp2", argv[1]="nomfichier.txt"
int main(int argc, const char** argv){
    // Gestion de l'entrée :
    //  - lecture depuis un fichier si un argument est spécifié;
    //  - sinon, lecture depuis std::cin.
    if(argc>1){
         std::ifstream entree_fichier(argv[1]);
         if(entree_fichier.fail()){
             std::cerr << "Erreur d'ouverture du fichier '" << argv[1] << "'" << std::endl;
             return 1;
         }
         return tp2(entree_fichier);
    }else
         return tp2(std::cin);        

    return 0;
}

